import Contact from "./layout/Contact";
import Footer from "./layout/Footer"
import Map from "./layout/Map";
import Prevention from "./layout/Prevention";
import Symptomps from "./layout/Symptomps";
import './App.scss'
import Home from "./pages/Home"
import Pages1 from "./pages/Pages1"

const App = () => {
  return (
    <>
      <div className="container">
        <Home />
        <Pages1 />
        <Symptomps/>
        <Prevention/>
        <Map/>
        <Contact/>
        <Footer/>
      </div>
    </>
  );
}

export default App;

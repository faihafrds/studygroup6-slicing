import LandingPage from "../component/LandingPage"
import MainNavbar from "../component/MainNavbar"

const Home = () => {
  return (
    <>
      <MainNavbar />
      <LandingPage />
    </>
  )
}

export default Home

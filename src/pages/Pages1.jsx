import { Col, Container, NavLink, Row } from "reactstrap"
import Pages1Pic from '../assets/img/Pages1Pic.svg'
import './Pages1.scss'

const Pages1 = () => {
  return (
    <>
      <Container className="themed-container " fluid=" xl">
        <Row className="d-flex align-items-center">
          <Col>
            <img src={Pages1Pic} className="img-fluid rotate" />
          </Col>
          <Col className="d-flex justify-content-start">
            <div className="Pages1Text d-flex flex-column ">
              <h3>What is Covid-19</h3>
              <h1>Coronavirus</h1>
              <p>Corona viruses are a type of virus. There are many different kinds, and <br />
              some cause disease. A newly identified type has caused a recent <br />
              outbreak of respiratory illness now called COVID-19.Lauren Sauer, M.S., <br />
              the director of operations with the Johns Hopkins Office of Critical Event <br />
               Preparedness and Response</p>
              <div className="LandingBtn d-flex">
                <NavLink href="#" className="btn-second">Learn More</NavLink>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Pages1

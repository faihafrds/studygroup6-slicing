import Maps from '../assets/Map.svg'
import Report from '../assets/Report.svg'

const Map = () => {
    return (
        <div>
            <div className="row mt-4">
                <div className="col-8">
                    <img src={Maps} alt="" srcset="" className="img-fluid"/>
                </div>
                <div className="col-4">
                    <img src={Report} alt="" srcset="" className="img-fluid"/>
                </div>
            </div>
        </div>
    )
}

export default Map

import Logo from '../assets/Logo.svg'
import Sosmed from '../assets/Sosmed.svg'
import { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

const Footer = () => {
    const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
    return (
        <div>
            <div className="row mt-3">
                <Navbar color="white" light expand="md">
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar className="justify-content-between">
                    <Nav navbar>
                    <NavbarBrand href="/"><img src={Logo} alt="Logo" /></NavbarBrand>
                    </Nav>  
                    <Nav navbar >
                    <NavItem>
                        <NavLink href="#" className="text-grey ms">About</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" className="text-grey ms">Features</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" className="text-grey ms">Pricing</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" className="text-grey ms">Testimonials</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="#" className="text-grey ms">Help</NavLink>
                    </NavItem>
                    </Nav>
                    <Nav navbar>
                    <NavbarBrand href="/"><img src={Sosmed} alt="Logo" /></NavbarBrand>
                    </Nav>
                </Collapse>
                </Navbar>
            </div>
            <div className="row text-center">
                <p>2020 @ All rights reserved by pixelspark.co</p>
            </div>
        </div>
    )
}

export default Footer

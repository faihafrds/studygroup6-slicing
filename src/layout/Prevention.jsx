import MaskGirl from '../assets/MaskGirl.svg'
import Bullet1 from '../assets/Bullet1.svg'
import Bullet2 from '../assets/Bullet2.svg'
import Bullet3 from '../assets/Bullet3.svg'
import Bullet4 from '../assets/Bullet4.svg'
import WashHand from '../assets/Mask.svg'
import NoseRog from '../assets/NoseRog.svg'
import AvoidContact from '../assets/AvoidContact.svg'
import StepBanner from '../components/StepBanner'

const Prevention = () => {
    return (
        <div>
            <div className="row text-center mb-3">
                <h5>Covid-19</h5>
                <h3>What Should We Do</h3>
                <p>Corona viruses are a type of virus. There are many different kinds, and some cause disease. <br/> A newly identified type has caused</p>
            </div>
            <div className="row">
                <div className="col-6">
                <StepBanner 
                number={Bullet1} 
                title="Wear Masks"
                />
                </div>
                <div className="col-6 text-center">
                    <img src={MaskGirl} alt="" srcset="" className="img-fluid" width="70%"/>
                </div>
            </div>
            <div className="row">
                <div className="col-6 text-center">
                    <img src={WashHand} alt="" srcset="" className="img-fluid" width="70%"/>
                </div>
                <div className="col-6">
                <StepBanner 
                number={Bullet2} 
                title="Wash Your Hands"
                />
                </div>
            </div>
            <div className="row">
                <div className="col-6">
                <StepBanner 
                number={Bullet3} 
                title="Use Nose - Rog"
                />
                </div>
                <div className="col-6 text-center">
                    <img src={NoseRog} alt="" srcset="" className="img-fluid" width="70%"/>
                </div>
            </div>
            <div className="row">
                <div className="col-6 text-center">
                    <img src={AvoidContact} alt="" srcset="" className="img-fluid" width="70%"/>
                </div>
                <div className="col-6">
                <StepBanner 
                number={Bullet4} 
                title="Avoid Contacts"
                />
                </div>
            </div>
        </div>
    )
}

export default Prevention

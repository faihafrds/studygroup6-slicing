const Contact = () => {
    return (
        <div>
            <div className="row justify-content-center text-center">
                <div className="col-5">
                <h3>Have Question in mind? <br/> Let us help you</h3>
                <input className="form-control" type="text" placeholder="snmonydemo@gmail.com"/>
                <button className="btn btn-danger">Send</button>
                </div>
            </div>
        </div>
    )
}

export default Contact

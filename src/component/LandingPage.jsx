import { Col, Container, NavLink, Row } from "reactstrap"
import Illustration1 from '../assets/img/Illustration1.svg'
import './LandingPage.scss'

const LandingPage = () => {
  return (
    <>
      <Container className="themed-container " fluid=" xl">
        <Row className="d-flex align-items-center">
          <Col className="d-flex justify-content-end">
            <div className="LandingText d-flex flex-column ">
              <h3>Covid Alert</h3>
              <h1>Stay at Home quarantine <br />
                  To stop Corona virus</h1>
              <p>There is no specific medicine to prevent or treat coronavirus <br /> disease (COVID-19). People may need supportive care to .</p>
              <div className="LandingBtn d-flex">
                <NavLink href="#" className="btn-primary">Let Us Help</NavLink>
              </div>
            </div>
          </Col>
          <Col>
            <img src={Illustration1} className="img-fluid" />
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default LandingPage

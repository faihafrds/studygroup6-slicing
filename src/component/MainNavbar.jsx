import { useState } from 'react';
import {
  Collapse,
  Navbar,

  NavbarBrand,
  Nav,
  NavItem,
  NavLink,

  Container
} from 'reactstrap';
import Logo from '../assets/img/Logo.svg'
import './MainNavbar.scss'

const MainNavbar = () => {
  return (
    <div>
      <Container>
        <Navbar color="white" light expand="md" className="d-flex justify-content-between">
          <NavbarBrand href="/" className="mr-auto"><img src={Logo} alt="" /></NavbarBrand>
          <div navbar>
            <Nav navbar>
              <NavItem>
                <NavLink href="/components/">Overview</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Contagion</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Symptoms</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Prevention</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#" className="btn-outline">Contact</NavLink>
              </NavItem>
            </Nav>
          </div>
        </Navbar>
      </Container>
    </div >
  );
}

export default MainNavbar

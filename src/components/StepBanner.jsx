const StepBanner = (props) => {
    return (
        <div>
            <div className="row mt-5 pt-5">
                <div className="col-2">
                    <img src={props.number} alt="" srcset=""/>
                </div>
                <div className="col-8">
                    <h4>{props.title}</h4>
                    <p>Continually seize impactful vortals rather than future-proof supply chains. Uniquely exploit emerging niches via fully tested meta-services. Competently pursue standards compliant leadership skills vis-a-vis pandemic "outside the box" thinking. Objectively</p>
                </div>
            </div>
        </div>
    )
}

export default StepBanner
